import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        var input = scanner.nextLine().split(" ");
        int a = Integer.parseInt(input[0]);
        int b = Integer.parseInt(input[1]);
        int c = Integer.parseInt(input[2]);
        int count = 0;

        for (int i = a; i <= b; i++) {
            if (i % c == 0) {
                count++;
            }
        }

        System.out.print(count);
    }
}
