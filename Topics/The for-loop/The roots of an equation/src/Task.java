import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);

        final int a = scanner.nextInt();
        final int b = scanner.nextInt();
        final int c= scanner.nextInt();
        final int d = scanner.nextInt();
        double result = 0;

        List<Double> roots = new ArrayList<>();

        for (double i = 0; i < 1001; i++) {
            result = a * Math.pow(i, 3) +
                    b * Math.pow(i, 2) +
                    c * i +
                    d;

            if (result == 0) {
                roots.add(i);
            }

            if (roots.size() >= 3) {
                break;
            }
        }

        roots.stream().sorted().forEach(r -> System.out.println(Math.round(r)));
    }
}
