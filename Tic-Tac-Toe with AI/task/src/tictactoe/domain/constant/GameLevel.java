package tictactoe.domain.constant;

public enum GameLevel {
    EASY,
    MEDIUM,
    HARD;
}
