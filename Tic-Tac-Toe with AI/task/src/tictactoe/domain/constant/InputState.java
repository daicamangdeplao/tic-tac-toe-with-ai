package tictactoe.domain.constant;

public enum InputState {
    INVALID_INPUT,
    OUT_OF_RANGE,
    OCCUPIED,
    VALID;
}
