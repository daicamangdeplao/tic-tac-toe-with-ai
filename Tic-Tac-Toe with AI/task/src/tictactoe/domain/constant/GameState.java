package tictactoe.domain.constant;

public enum GameState {
    GAME_NOT_FINISHED,
    DRAW,
    X_WINS,
    O_WINS;
}
