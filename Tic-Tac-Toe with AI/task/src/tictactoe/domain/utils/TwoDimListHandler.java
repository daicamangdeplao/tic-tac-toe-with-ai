package tictactoe.domain.utils;

import tictactoe.domain.Board;

import java.util.ArrayList;
import java.util.List;

public class TwoDimListHandler {
    public static List<String> getRow(int rowIndex, List<List<String>> twoDimList) {
        return new ArrayList<>(twoDimList.get(rowIndex));
    }

    public static List<String> getColumn(int columnIndex, List<List<String>> twoDimList) {
        List<String> column = new ArrayList<>();
        for (List<String> strings : twoDimList) {
            column.add(strings.get(columnIndex));
        }
        return column;
    }

    public static List<String> getMainDiagonal(List<List<String>> state) {
        List<String> mainDiagonal = new ArrayList<>();
        for (int row = 0; row < state.size(); row++) {
            mainDiagonal.add(state.get(row).get(row));
        }
        return mainDiagonal;
    }

    public static List<String> getAntiDiagonal(List<List<String>> state) {
        List<String> mainDiagonal = new ArrayList<>();
        for (int row = 0; row < state.size(); row++) {
            mainDiagonal.add(state.get(row).get(state.size() - row - 1));
        }
        return mainDiagonal;
    }
}
