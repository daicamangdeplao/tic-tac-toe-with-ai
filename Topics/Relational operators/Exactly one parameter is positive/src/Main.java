import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here

        final int a = scanner.nextInt();
        final int b = scanner.nextInt();
        final int c = scanner.nextInt();

        if ((a > 0 && b <= 0 && c <= 0) ||
                (b > 0 && a <= 0 && c <= 0) ||
                (c > 0 && b <= 0 && a <= 0)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
