import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();

        int[] numbers = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            numbers[i] = scanner.nextInt();
        }

        int criteria = scanner.nextInt();
        int sum = 0;
        for (int number : numbers) {
            if (number > criteria) {
                sum = sum + number;
            }
        }

        System.out.println(sum);
    }
}
