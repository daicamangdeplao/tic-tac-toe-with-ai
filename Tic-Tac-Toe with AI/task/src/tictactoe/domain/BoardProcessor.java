package tictactoe.domain;

import tictactoe.domain.constant.ApplicationState;
import tictactoe.domain.constant.GameLevel;
import tictactoe.domain.constant.GameState;
import tictactoe.domain.constant.InputState;
import tictactoe.domain.player.Player;
import tictactoe.domain.player.PlayerMachine;
import tictactoe.domain.utils.UserInputHandler;

import java.util.List;

import static tictactoe.domain.utils.UserInputHandler.scanner;

public class BoardProcessor {

    private static final String SPACE_STRING = " ";
    private final Board board;

    public BoardProcessor(String initialState) {
        this.board = new Board(initialState);
    }

    public Board getBoard() {
        return board;
    }

    public void showBoard() {
        this.board.showBoard();
    }

    public ApplicationState updateBoard(Player playerA, Player playerB) {
        if (isPlayerAbleToMakeANewMove()) {
            makeAMove(playerA);
        }
        
        if (isPlayerAbleToMakeANewMove()) {
            makeAMove(playerB);
        }

        return checkState();
    }

    private boolean isPlayerAbleToMakeANewMove() {
        return board.isFinished() == GameState.GAME_NOT_FINISHED;
    }

    private void makeAMove(Player player) {
        List<Integer> coordinates;
        InputState inputState;

        if (player instanceof PlayerMachine) {
            if (((PlayerMachine) player).getGameLevel() == GameLevel.EASY) {
                System.out.println("Making move level \"easy\"");
            } else if (((PlayerMachine) player).getGameLevel() == GameLevel.MEDIUM) {
                System.out.println("Making move level \"medium\"");
            } else {
                System.out.println("Making move level \"hard\"");
            }
            do {
                // TODO makeAMove(this.getBoard())
                coordinates = player.getCoordinates(this.getBoard());
//                coordinates = player.thinkLevelMedium(this.getBoard());
            } while (!isValidRandomCoordinates(coordinates));
        } else {
            String inputCoordinates;
            do {
                System.out.print("Enter the coordinates: ");
                inputCoordinates = scanner.nextLine();
                inputState = UserInputHandler.checkInPutCoordinates(this, inputCoordinates);
            } while (inputState != InputState.VALID);
            player.setCoordinates(inputCoordinates);
            coordinates = player.getCoordinates(this.board);
        }

        board.update(coordinates.get(0), coordinates.get(1), player.getLabel());
        showBoard();
    }

    private ApplicationState checkState() {
        if (this.board.isFinished() == GameState.X_WINS) {
            System.out.println("X wins");
            return ApplicationState.EXIT;
        }
        if (this.board.isFinished() == GameState.O_WINS) {
            System.out.println("O wins");
            return ApplicationState.EXIT;
        }
        if (this.board.isFinished() == GameState.DRAW) {
            System.out.println("Draw");
            return ApplicationState.EXIT;
        }
        return ApplicationState.CONTINUE;
    }

    private boolean isValidRandomCoordinates(List<Integer> coordinates) {
        // A valid random coordinates is a place where not occupied
        return board.getState().get(coordinates.get(0)).get(coordinates.get(1)).equals(SPACE_STRING);
    }
}
