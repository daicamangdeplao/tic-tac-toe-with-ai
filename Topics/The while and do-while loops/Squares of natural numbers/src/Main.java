import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        int n = scanner.nextInt();
        int iterator = 1;
        double nextSquared = 0;

        do {
            nextSquared = Math.pow(iterator, 2);
            if (nextSquared > n) {
                break;
            }
            System.out.println((int) nextSquared);
            iterator++;
        } while(true);
    }
}
