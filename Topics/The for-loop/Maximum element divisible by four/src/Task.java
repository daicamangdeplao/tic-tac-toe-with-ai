// write your answer here 

import java.util.*;
import java.util.stream.Collectors;

public class Task {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        int n = Integer.parseInt(scanner.nextLine());
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            numbers.add(Integer.parseInt(scanner.nextLine()));
        }

        List<Integer> filteredNumbers = numbers.stream()
                .filter(v -> v % 4 == 0)
                .collect(Collectors.toList());

        int max = filteredNumbers.stream()
                .max(Comparator.comparingInt(v -> v))
                .orElseThrow(() -> new NoSuchElementException(""));
        System.out.println(max);
    }
}
