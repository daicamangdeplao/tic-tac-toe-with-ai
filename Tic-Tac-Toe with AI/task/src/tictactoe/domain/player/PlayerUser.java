package tictactoe.domain.player;

import tictactoe.domain.Board;

import java.util.ArrayList;
import java.util.List;

public class PlayerUser implements Player {
    private final String label;
    private String coordinates;

    public PlayerUser(String coordinates, String label) {
        this.label = label;
        this.coordinates = coordinates;
    }

    @Override
    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public List<Integer> getCoordinates(Board board) {
        String[] inputs = coordinates.split(" ");
        return new ArrayList<>(
                List.of(Integer.parseInt(inputs[0]) - 1, Integer.parseInt(inputs[1]) - 1)
        );
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public List<Integer> thinkLevelMedium(Board board) {
        return null;
    }

}
