import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here
        final String input = scanner.next();
        for (int i = 0; i < input.length(); i++) {
            final char printedChar = input.charAt((i - (getFinalIndex(input))) * -1);
            if (!isFirstPrintedChar(input, printedChar) || !isZeroCharacter(printedChar)) {
                System.out.print(String.valueOf(printedChar));
            }
        }
    }
    
    public static boolean isFirstPrintedChar(String input, char c) {
        return c == input.charAt(getFinalIndex(input));
    }

    public static boolean isZeroCharacter(char c) {
        return c == '0';
    }

    private static int getFinalIndex(String input) {
        return input.length() - 1;
    }
}
