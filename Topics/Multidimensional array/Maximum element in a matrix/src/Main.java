import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        List<Integer> matrixSizes = readMatrixSize(scanner);
        List<List<Integer>> matrix = readMatrixElements(scanner, matrixSizes);

        int max = findMaximum(matrix);
        List<Integer> indices = findFirstIndices(max, matrix, matrixSizes);
        System.out.println(indices.get(0) + " " + indices.get(1));
    }

    private static List<Integer> findFirstIndices(int max, List<List<Integer>> matrix, List<Integer> matrixSizes) {
        int rows = matrixSizes.get(0);
        int cols = matrixSizes.get(1);

        List<Integer> indices = new ArrayList<>();
        boolean found = false;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix.get(i).get(j) == max) {
                    indices.add(i);
                    indices.add(j);
                    found = true;
                    break;
                }
            }
            if (found) {
                break;
            }
        }
        return indices;
    }

    private static int findMaximum(List<List<Integer>> matrix) {
        return matrix.stream()
                .flatMap(Collection::stream)
                .max(Integer::compareTo)
                .orElseThrow(RuntimeException::new);
    }

    private static List<List<Integer>> readMatrixElements(Scanner scanner, List<Integer> matrixSizes) {
        int rows = matrixSizes.get(0);
        List<List<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < rows; i++) {
            matrix.add(
                    Arrays.stream(scanner.nextLine().split(" "))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList())
            );
        }

        return matrix;
    }

    private static List<Integer> readMatrixSize(Scanner scanner) {
        return Arrays.stream(scanner.nextLine().split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }
}
