package tictactoe.domain.player;


import tictactoe.domain.Board;

import java.util.List;

public interface Player {
    void setCoordinates(String coordinates);
    List<Integer> getCoordinates(Board board);
    String getLabel();

    List<Integer> thinkLevelMedium(Board board);
}
