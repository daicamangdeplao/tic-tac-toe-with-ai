package tictactoe;

import tictactoe.domain.BoardProcessor;
import tictactoe.domain.constant.ApplicationState;
import tictactoe.domain.constant.GameLevel;
import tictactoe.domain.player.Player;
import tictactoe.domain.player.PlayerMachine;
import tictactoe.domain.player.PlayerUser;
import tictactoe.domain.utils.UserInputHandler;

public class Main {

    static final String INITIAL_STATE = "_________";
//    static final String INITIAL_STATE = "___XO____";
//    static final String INITIAL_STATE = "O_XX_X_OO";

    static BoardProcessor boardProcessor;
    private static Player playerA;
    private static Player playerB;

    public static void initialize(ApplicationState applicationState) {
        if (applicationState == ApplicationState.EASY_EASY) {
            playerA = new PlayerMachine("X", GameLevel.EASY);
            playerB = new PlayerMachine("O", GameLevel.EASY);
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.EASY_USER) {
            playerA = new PlayerMachine("X", GameLevel.EASY);
            playerB = new PlayerUser("", "O");
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.USER_EASY) {
            playerA = new PlayerUser("", "X");
            playerB = new PlayerMachine("O", GameLevel.EASY);
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.MEDIUM_USER) {
            playerA = new PlayerMachine("X", GameLevel.MEDIUM);
            playerB = new PlayerUser("", "O");
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.USER_MEDIUM) {
            playerA = new PlayerUser("", "X");
            playerB = new PlayerMachine("O", GameLevel.MEDIUM);
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.HARD_USER) {
            playerA = new PlayerMachine("X", GameLevel.HARD);
            playerB = new PlayerUser("", "O");
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.USER_HARD) {
            playerA = new PlayerUser("", "X");
            playerB = new PlayerMachine("O", GameLevel.HARD);
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.HARD_HARD) {
            playerA = new PlayerMachine("X", GameLevel.HARD);
            playerB = new PlayerMachine("O", GameLevel.HARD);
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }

        if (applicationState == ApplicationState.USER_USER) {
            playerA = new PlayerUser("", "X");
            playerB = new PlayerUser("", "O");
            boardProcessor = new BoardProcessor(INITIAL_STATE);
            boardProcessor.showBoard();
        }
    }

    public static void main(String[] args) {
        // write your code here
        ApplicationState applicationState;

        do {
            applicationState = UserInputHandler.handleArguments();
        } while (!isArgumentValid(applicationState));

        initialize(applicationState);

        if (applicationState != ApplicationState.EXIT) {
            run();
        }
    }

    private static void run() {
        ApplicationState applicationState;

        do {
            applicationState = boardProcessor.updateBoard(playerA, playerB);
        } while (applicationState != ApplicationState.EXIT);
    }

    private static boolean isArgumentValid(ApplicationState retCode) {
        return retCode == ApplicationState.EASY_EASY ||
                retCode == ApplicationState.USER_USER ||
                retCode == ApplicationState.EASY_USER ||
                retCode == ApplicationState.USER_EASY ||
                retCode == ApplicationState.MEDIUM_USER ||
                retCode == ApplicationState.USER_MEDIUM ||
                retCode == ApplicationState.HARD_USER ||
                retCode == ApplicationState.USER_HARD ||
                retCode == ApplicationState.HARD_HARD ||
                retCode == ApplicationState.EXIT;
    }
}
