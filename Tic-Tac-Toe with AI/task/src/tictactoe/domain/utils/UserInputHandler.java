package tictactoe.domain.utils;

import tictactoe.domain.BoardProcessor;
import tictactoe.domain.constant.ApplicationState;
import tictactoe.domain.constant.InputState;

import java.util.Optional;
import java.util.Scanner;

public class UserInputHandler {
    static final String SPACE_STRING = " ";
    static final String EMPTY_STRING = "";
    static final String BAD_PARAMETERS_MSG = "Bad parameters!";

    public static final Scanner scanner = new Scanner(System.in);

    public static InputState checkInPutCoordinates(BoardProcessor boardProcessor, String inputCoordinates) {
        String[] inputs = inputCoordinates.split(SPACE_STRING);
        InputState inputState = InputState.VALID;

        Optional<InputState> invalidInput = isInvalidInput(inputs);
        if (invalidInput.isPresent()) return invalidInput.get();

        Optional<InputState> outOfRangeInput = isOutOfRangeInput(inputs);
        if (outOfRangeInput.isPresent()) return outOfRangeInput.get();

        Optional<InputState> occupiedInput = isOccupiedInput(inputs, boardProcessor);
        if (occupiedInput.isPresent()) return occupiedInput.get();

        return inputState;
    }

    public static ApplicationState handleArguments() {
        System.out.print("Input command: ");
        String[] arguments = scanner.nextLine().split(" ");

        if (arguments[0].equals("exit")) {
            return ApplicationState.EXIT;
        }

        if (
                arguments.length != 3 ||
                        !arguments[0].equals("start") ||
                        (!arguments[1].equals("user") && !arguments[1].equals("easy") && !arguments[1].equals("medium") && !arguments[1].equals("hard")) ||
                        (!arguments[2].equals("user") && !arguments[2].equals("easy") && !arguments[2].equals("medium") && !arguments[2].equals("hard"))
        ) {
            System.out.println(BAD_PARAMETERS_MSG);
            return ApplicationState.BAD_PARAMETER;
        }

        if (arguments[1].equals("user") && arguments[2].equals("easy")) {
            return ApplicationState.USER_EASY;
        }

        if (arguments[1].equals("easy") && arguments[2].equals("user")) {
            return ApplicationState.EASY_USER;
        }

        if (arguments[1].equals("user") && arguments[2].equals("medium")) {
            return ApplicationState.USER_MEDIUM;
        }

        if (arguments[1].equals("medium") && arguments[2].equals("user")) {
            return ApplicationState.MEDIUM_USER;
        }

        if (arguments[1].equals("user") && arguments[2].equals("hard")) {
            return ApplicationState.USER_HARD;
        }

        if (arguments[1].equals("hard") && arguments[2].equals("user")) {
            return ApplicationState.HARD_USER;
        }

        if (arguments[1].equals("hard") && arguments[2].equals("hard")) {
            return ApplicationState.HARD_HARD;
        }

        if (arguments[1].equals("easy") && arguments[2].equals("easy")) {
            return ApplicationState.EASY_EASY;
        }

        if (arguments[1].equals("user") && arguments[2].equals("user")) {
            return ApplicationState.USER_USER;
        }

        return ApplicationState.START;
    }

    public static ApplicationState handleArgumentsXX() {
        System.out.print("Input command: ");

        String arg1 = scanner.next();
        String arg2 = scanner.next();
        String arg3 = scanner.next();

        if (arg1.equals("exit")) {
            return ApplicationState.EXIT;
        }

        if (!arg1.equals("start") ||
                        (!arg2.equals("user") && !arg2.equals("easy") && !arg2.equals("medium") && !arg2.equals("hard")) ||
                        (!arg3.equals("user") && !arg3.equals("easy") && !arg3.equals("medium") && !arg3.equals("hard"))
        ) {
            System.out.println(BAD_PARAMETERS_MSG);
            return ApplicationState.BAD_PARAMETER;
        }

        if (arg2.equals("user") && arg3.equals("easy")) {
            return ApplicationState.USER_EASY;
        }

        if (arg2.equals("easy") && arg3.equals("user")) {
            return ApplicationState.EASY_USER;
        }

        if (arg2.equals("user") && arg3.equals("medium")) {
            return ApplicationState.USER_MEDIUM;
        }

        if (arg2.equals("medium") && arg3.equals("user")) {
            return ApplicationState.MEDIUM_USER;
        }

        if (arg2.equals("user") && arg3.equals("hard")) {
            return ApplicationState.USER_HARD;
        }

        if (arg2.equals("hard") && arg3.equals("user")) {
            return ApplicationState.HARD_USER;
        }

        if (arg2.equals("hard") && arg3.equals("hard")) {
            return ApplicationState.HARD_HARD;
        }

        if (arg2.equals("easy") && arg3.equals("easy")) {
            return ApplicationState.EASY_EASY;
        }

        if (arg2.equals("user") && arg3.equals("user")) {
            return ApplicationState.USER_USER;
        }

        return ApplicationState.START;
    }

    private static Optional<InputState> isOccupiedInput(String[] inputs, BoardProcessor boardProcessor) {
        if (!isAvailable(inputs, boardProcessor)) {
            System.out.println("This cell is occupied! Choose another one!");
            return Optional.of(InputState.OCCUPIED);
        }
        return Optional.empty();
    }

    private static Optional<InputState> isOutOfRangeInput(String[] inputs) {
        for (String input : inputs) {
            if (!isInRange(input)) {
                System.out.println("Coordinates should be from 1 to 3!");
                return Optional.of(InputState.OUT_OF_RANGE);
            }
        }
        return Optional.empty();
    }

    private static Optional<InputState> isInvalidInput(String[] inputs) {
        for (String input : inputs) {
            if (!isNumeric(input)) {
                System.out.println("You should enter numbers!");
                return Optional.of(InputState.INVALID_INPUT);
            }
        }
        return Optional.empty();
    }

    private static boolean isAvailable(String[] inputs, BoardProcessor boardProcessor) {
        int row = Integer.parseInt(inputs[0]);
        int col = Integer.parseInt(inputs[1]);

        String queryingValue;
        try {
            queryingValue = boardProcessor.getBoard().getState().get(row - 1).get(col - 1);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return isSpaceString(queryingValue);
    }

    private static boolean isInRange(String input) {
        int index = Integer.parseInt(input) - 1;
        return index < 3 && index >= 0;
    }

    private static boolean isNumeric(String input) {
        if (input == null || isEmptyString(input)) {
            return false;
        }
        try {
            Double.parseDouble(input);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private static boolean isSpaceString(String value) {
        return value.equals(SPACE_STRING);
    }

    private static boolean isEmptyString(String value) {
        return value.equals(EMPTY_STRING);
    }
}
