package tictactoe.domain.player;

import tictactoe.domain.Board;
import tictactoe.domain.constant.GameLevel;
import tictactoe.domain.utils.TwoDimListHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class PlayerMachine implements Player{
    private final String label;
    private long seed;

    String aiPlayer;
    String huPlayer;

    public GameLevel getGameLevel() {
        return gameLevel;
    }

    private final GameLevel gameLevel;

    public PlayerMachine(String label, GameLevel gameLevel) {
        this.label = label;
        this.gameLevel = gameLevel;
        seed = System.nanoTime();
        aiPlayer = label.equals("X") ? "X" : "O";
        huPlayer = aiPlayer.equals("O") ? "X" : "O";
    }

    @Override
    public void setCoordinates(String coordinates) {

    }

    @Override
    public List<Integer> getCoordinates(Board board) {
        if (this.gameLevel == GameLevel.EASY) {
            return randomizeCoordinates();
        }

        if (this.gameLevel == GameLevel.MEDIUM) {
            return thinkLevelMedium(board);
        }

        return thinkLevelHard(board);
    }

    private List<Integer> thinkLevelHard(Board board) {

        List<String> flatBoard = board.getState().stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());

        for (int i = 0; i < flatBoard.size(); i++) {
            if (flatBoard.get(i).equals(" ")) flatBoard.set(i, String.valueOf(i));
        }

//        List<Integer> availSpots = emptyIndices(flatBoard);
        Move ret = minimax(flatBoard, aiPlayer, -1);

        Map<Integer, List<Integer>> temp = Map.of(
                0, List.of(0, 0),
                1, List.of(0, 1),
                2, List.of(0, 2),
                3, List.of(1, 0),
                4, List.of(1, 1),
                5, List.of(1, 2),
                6, List.of(2, 0),
                7, List.of(2, 1),
                8, List.of(2, 2)
        );

        return temp.get(ret.index);
    }

    private Move minimax(List<String> newBoard, String player, int index) {
        //available spots => [1, 4, 6]
        List<Integer> availSpots = emptyIndices(newBoard);

        // checks for the terminal states such as win, lose, and tie
        //and returning a value accordingly
        if (winning(newBoard, huPlayer)) {
            return new Move(index, -10);
        }
        else if (winning(newBoard, aiPlayer)) {
            return new Move(index, 10);
        }
        else if (availSpots.isEmpty()) {
            return new Move(index, 0);
        }

        // an array to collect all the calculating moves (object)
        List<Move> moves = new ArrayList<>();

        // loop through available spots
        for (Integer availSpot : availSpots) {
            //create an object for each and store the index of that spot
            Move move = new Move();
            move.index = Integer.parseInt(newBoard.get(availSpot));

            // set the empty spot to the current player
            newBoard.set(availSpot, player);

        /*collect the score resulted from calling minimax
          on the opponent of the current player*/
            Move result;
            if (player.equals(aiPlayer)) {
                result = minimax(newBoard, huPlayer, move.index);
            } else {
                result = minimax(newBoard, aiPlayer, move.index);
            }
            move.score = result.score;

            // reset the spot to empty
            newBoard.set(availSpot, String.valueOf(move.index));

            // push the object to the array
            moves.add(move);
        }

        // if it is the computer's turn loop over the moves and choose the move with the highest score
        int bestMove = 0;
        if (player.equals(aiPlayer)) {
            var bestScore = -10000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score > bestScore) {
                    bestScore = moves.get(i).score;
                    bestMove = moves.get(i).index;
                }
            }
        } else {

            // else loop over the moves and choose the move with the lowest score
            var bestScore = 10000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score < bestScore) {
                    bestScore = moves.get(i).score;
                    bestMove = moves.get(i).index;
                }
            }
        }

        // return the chosen move (object) from the moves array
        int finalBestMove = bestMove;
        return moves.stream().filter(m -> m.index == finalBestMove).findFirst().get();
    }

    private List<Integer> emptyIndices(List<String> flatBoard) {
        return flatBoard.stream()
                .filter(value -> !value.equals("X") && !value.equals("O"))
                .map(flatBoard::indexOf)
                .collect(Collectors.toList());
    }

    private boolean winning(List<String> flatBoard, String player) {
        if (
                (flatBoard.get(0).equals(player) && flatBoard.get(1).equals(player) && flatBoard.get(2).equals(player)) ||
                        (flatBoard.get(3).equals(player) && flatBoard.get(4).equals(player) && flatBoard.get(5).equals(player)) ||
                        (flatBoard.get(6).equals(player) && flatBoard.get(7).equals(player) && flatBoard.get(8).equals(player)) ||
                        (flatBoard.get(0).equals(player) && flatBoard.get(3).equals(player) && flatBoard.get(6).equals(player)) ||
                        (flatBoard.get(1).equals(player) && flatBoard.get(4).equals(player) && flatBoard.get(7).equals(player)) ||
                        (flatBoard.get(2).equals(player) && flatBoard.get(5).equals(player) && flatBoard.get(8).equals(player)) ||
                        (flatBoard.get(0).equals(player) && flatBoard.get(4).equals(player) && flatBoard.get(8).equals(player)) ||
                        (flatBoard.get(2).equals(player) && flatBoard.get(4).equals(player) && flatBoard.get(6).equals(player))
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public List<Integer> thinkLevelMedium(Board board) {
        int potentialAttackRow = spotTheAttackRow(board);
        if (foundTheSpot(potentialAttackRow)) {
            return takeActionOnRow(potentialAttackRow, board);
        }

        int potentialDefenseRow = spotTheDefenseRow(board);
        if (foundTheSpot(potentialDefenseRow)) {
            return takeActionOnRow(potentialDefenseRow, board);
        }

        int potentialDefenseCol = spotTheDefenseColumn(board);
        if (foundTheSpot(potentialDefenseCol)) {
            return takeActionOnCol(potentialDefenseCol, board);
        }

        int potentialDefenseMainDiagonal = spotTheDefenseMainDiagonal(board);
        if (foundTheSpot(potentialDefenseMainDiagonal)) {
            return takeActionOnMainDiagonal(potentialDefenseMainDiagonal);
        }

        int potentialDefenseAntiDiagonal = spotTheDefenseAntiDiagonal(board);
        if (foundTheSpot(potentialDefenseAntiDiagonal)) {
            return takeActionOnAntiDiagonal(potentialDefenseAntiDiagonal, board.getBoardSize());
        }

        return randomizeCoordinates();
    }

    private List<Integer> takeActionOnMainDiagonal(int index) {
        return new ArrayList<>(
                List.of(index, index)
        );
    }

    private List<Integer> takeActionOnAntiDiagonal(int index, int boardSize) {
        return new ArrayList<>(
                List.of(index, boardSize - index - 1)
        );
    }

    private int spotTheAttackRow(Board board) {
        for (int row = 0; row < board.getBoardSize(); row++) {
            List<String> rowBoard = TwoDimListHandler.getRow(row, board.getState());
            if (existTwoSameMoves(rowBoard)) {
                return row;
            }
        }
        return -1;
    }

    private boolean foundTheSpot(int potentialRow) {
        return potentialRow != -1;
    }

    // TODO introduce enum Direction = {ROW, COLUMN}
    private List<Integer> takeActionOnRow(int row, Board board) {
        return new ArrayList<>(
                List.of(
                        row,
                        TwoDimListHandler.getRow(row, board.getState()).indexOf(" ")
                )
        );
    }

    private List<Integer> takeActionOnCol(int col, Board board) {
        return new ArrayList<>(
                List.of(
                        TwoDimListHandler.getColumn(col, board.getState()).indexOf(" "),
                        col
                )
        );
    }

    // TODO replace with predicate
    private int spotTheDefenseRow(Board board) {
        for (int row = 0; row < board.getBoardSize(); row++) {
            List<String> rowBoard = TwoDimListHandler.getRow(row, board.getState());
            if (existTwoRivalMoves(rowBoard)) {
                return row;
            }
        }
        return -1;
    }

    private boolean existTwoRivalMoves(List<String> rowBoard) {
        String rivalLabel = this.label.equals("X") ? "O" : "X";
        return rowBoard.stream().filter(s -> s.equals(rivalLabel)).count() == 2 &&
                rowBoard.contains(" ");
    }

    private boolean existTwoSameMoves(List<String> rowBoard) {
        return rowBoard.stream().filter(s -> s.equals(this.label)).count() == 2 &&
                rowBoard.contains(" ");
    }

    private int spotTheDefenseColumn(Board board) {
        for (int col = 0; col < board.getBoardSize(); col++) {
            List<String> colBoard = TwoDimListHandler.getColumn(col, board.getState());
            if (existTwoRivalMoves(colBoard)) {
                return col;
            }
        }
        return -1;
    }

    private int spotTheDefenseMainDiagonal(Board board) {
        List<String> mainDiagonal = TwoDimListHandler.getMainDiagonal(board.getState());
        if (existTwoRivalMoves(mainDiagonal)) {
            return mainDiagonal.indexOf(" ");
        }
        return -1;
    }

    private int spotTheDefenseAntiDiagonal(Board board) {
        List<String> antiDiagonal = TwoDimListHandler.getAntiDiagonal(board.getState());
        if (existTwoRivalMoves(antiDiagonal)) {
            return antiDiagonal.indexOf(" ");
        }
        return -1;
    }

    private List<Integer> randomizeCoordinates() {
        List<Integer> coordinates = new ArrayList<>();
        seed = System.nanoTime();
        Random random = new Random(seed);
        int x = random.nextInt(3) ;
        int y = random.nextInt(3);
        coordinates.add(x);
        coordinates.add(y);
        return coordinates;
    }

    static class Move {
        int index;
        int score;

        public Move() {
        }

        public Move(int index, int score) {
            this.index = index;
            this.score = score;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
    }
}
