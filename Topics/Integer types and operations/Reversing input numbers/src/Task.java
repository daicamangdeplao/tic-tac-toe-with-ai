import java.util.Scanner;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        String[] inputArray = scanner.nextLine().split(" ");
        List<String> inputList = Arrays.asList(inputArray);
        Collections.reverse(inputList);

        for (String str : inputList) {
            System.out.print(str + " ");
        }

    }
}
