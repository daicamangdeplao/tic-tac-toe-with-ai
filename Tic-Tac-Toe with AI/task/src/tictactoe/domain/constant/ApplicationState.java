package tictactoe.domain.constant;

public enum ApplicationState {
    EXIT,
    BAD_PARAMETER,
    START,

    USER_USER,

    USER_EASY,
    EASY_USER,
    EASY_EASY,

    USER_MEDIUM,
    MEDIUM_USER,
    MEDIUM_MEDIUM,

    USER_HARD,
    HARD_USER,
    HARD_HARD,

    CONTINUE;
}
