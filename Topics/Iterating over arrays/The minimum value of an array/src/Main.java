import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> arrays = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            arrays.add(scanner.nextInt());
        }

        Integer min = arrays.stream().sorted().collect(Collectors.toList()).get(0);
        System.out.println(min);
    }
}
