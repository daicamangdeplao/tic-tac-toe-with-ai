type: edu
custom_name: stage4
files:
- name: src/tictactoe/Main.java
  visible: true
  text: |
    package tictactoe;

    public class Main {
        public static void main(String[] args) {
            // write your code here
        }
    }
  learner_created: false
- name: test/TicTacToeTest.java
  visible: false
  text: |
    import org.hyperskill.hstest.dynamic.DynamicTest;
    import org.hyperskill.hstest.stage.StageTest;
    import org.hyperskill.hstest.testcase.CheckResult;
    import org.hyperskill.hstest.testing.TestedProgram;

    import java.util.List;


    public class TicTacToeTest extends StageTest<String> {

        int[] easyAiMoves = new int[9];

        @DynamicTest(order = 0)
        CheckResult testBadParameters() {

            TestedProgram program = new TestedProgram();
            program.start();

            String output = program.execute("start");
            if (!output.toLowerCase().contains("bad parameters")) {
                return CheckResult.wrong("After entering start command with wrong parameters you should print 'Bad parameters!' and ask to enter a command again!");
            }

            output = program.execute("start easy");
            if (!output.toLowerCase().contains("bad parameters")) {
                return CheckResult.wrong("After entering start command with wrong parameters you should print 'Bad parameters!' and ask to enter a command again!");
            }

            program.execute("exit");

            if (!program.isFinished()) {
                return CheckResult.wrong("After entering 'exit' command you should stop the program!");
            }

            return CheckResult.correct();
        }


        @DynamicTest(order = 1)
        CheckResult testGridOutput() {

            TestedProgram program = new TestedProgram();

            program.start();

            String output = program.execute("start user easy");

            Grid printedGrid = Grid.fromOutput(output);
            Grid emptyGrid = Grid.fromLine("_________");

            if (!printedGrid.equals(emptyGrid)) {
                return CheckResult.wrong("After starting the program you should print an empty grid!\n" +
                    "Correct empty grid:\n" + emptyGrid);
            }

            if (!output.toLowerCase().contains("enter the coordinates:")) {
                return CheckResult.wrong("After printing an empty grid you should ask to enter cell coordinates!");
            }

            output = program.execute("2 2");

            Grid gridAfterMove = Grid.fromOutput(output);
            Grid correctGridAfterMove = Grid.fromLine("____X____");

            if (!gridAfterMove.equals(correctGridAfterMove)) {
                return CheckResult.wrong("After making the move wrong grid was printed.\n" +
                    "Your grid:\n" + gridAfterMove + "\n" +
                    "Correct grid:\n" + correctGridAfterMove);
            }

            if (!output.toLowerCase().replace("'", "\"").contains("making move level \"easy\"")) {
                return CheckResult.wrong("After entering a cell coordinates you should print:\nMaking move level \"easy\"");
            }

            Grid gridAfterAiMove = Grid.fromOutput(output, 2);

            if (gridAfterAiMove.equals(gridAfterMove)) {
                return CheckResult.wrong("After AI move grid wasn't changed!");
            }

            Grid gameGrid = gridAfterAiMove;

            while (true) {
                if (gameGrid.getGameState() != GameState.NOT_FINISHED) {
                    switch (gameGrid.getGameState()) {
                        case X_WIN:
                            if (!output.contains("X wins")) {
                                return CheckResult.wrong("You should print 'X wins' if X win the game");
                            }
                            break;
                        case O_WIN:
                            if (!output.contains("O wins")) {
                                return CheckResult.wrong("You should print 'O wins' if O win the game");
                            }
                            break;
                        case DRAW:
                            if (!output.contains("Draw")) {
                                return CheckResult.wrong("You should print 'Draw' if the game ends with draw!");
                            }
                            break;
                    }
                    break;
                }
                Position nextMove = Minimax.getMove(gameGrid, CellState.X);

                Grid tempGrid = gameGrid.copy();
                tempGrid.setCell(nextMove.x, nextMove.y, CellState.X);

                output = program.execute((nextMove.x + 1) + " " + (nextMove.y + 1));

                gameGrid = Grid.fromOutput(output);

                if (!gameGrid.equals(tempGrid)) {
                    return CheckResult.wrong("After making move (" + nextMove + ") the game grid is wrong!\n" +
                        "Your gird\n" + gameGrid + "\n" +
                        "Correct grid\n" + tempGrid);
                }

                if (gameGrid.getGameState() != GameState.NOT_FINISHED)
                    continue;

                gameGrid = Grid.fromOutput(output, 2);
            }

            return CheckResult.correct();
        }

        @DynamicTest(repeat = 100, order = 2)
        CheckResult checkEasyAi() {
            TestedProgram program = new TestedProgram();
            program.start();

            program.execute("start user easy");

            String output = program.execute("2 2");

            Grid gridAfterAiMove = Grid.fromOutput(output, 2);

            CellState[][] array = gridAfterAiMove.getGrid();

            for (int i = 0; i < 9; i++) {
                if (i == 4) {
                    continue;
                }
                if (array[i / 3][i % 3] == CellState.O) {
                    easyAiMoves[i]++;
                }
            }

            return CheckResult.correct();
        }

        @DynamicTest(order = 3)
        CheckResult checkRandom() {
            double averageScore = 0;

            for (int i = 0; i < easyAiMoves.length; i++) {
                averageScore += (i + 1) * easyAiMoves[i];
            }

            averageScore /= 8;

            double expectedValue = (double) (1 + 2 + 3 + 4 + 6 + 7 + 8 + 9) * 100 / 8 / 8;

            if (Math.abs(averageScore - expectedValue) > 20) {
                return CheckResult.wrong("Looks like your Easy level AI doesn't make a random move!");
            }

            return CheckResult.correct();
        }

        boolean isEasyNotMovingLikeMedium = false;

        @DynamicTest(repeat = 30, order = 4)
        CheckResult checkEasyNotMovingLikeMedium() {

            if (isEasyNotMovingLikeMedium) {
                return CheckResult.correct();
            }

            TestedProgram program = new TestedProgram();
            program.start();
            program.execute("start user easy");

            String output = program.execute("2 2");

            Grid gameGrid = Grid.fromOutput(output, 2);

            CellState[][] cellStates = gameGrid.getGrid();

            if (cellStates[0][0] == CellState.EMPTY && cellStates[2][2] == CellState.EMPTY) {
                output = program.execute("1 1");
                gameGrid = Grid.fromOutput(output, 2);
                if (gameGrid.getGrid()[2][2] == CellState.EMPTY) {
                    isEasyNotMovingLikeMedium = true;
                }
            } else {
                output = program.execute("1 3");
                gameGrid = Grid.fromOutput(output, 2);
                if (gameGrid.getGrid()[2][0] == CellState.EMPTY) {
                    isEasyNotMovingLikeMedium = true;
                }
            }

            program.stop();
            return CheckResult.correct();
        }

        @DynamicTest(order = 5)
        CheckResult checkEasyNotMovingLikeMediumAfter() {
            if (!isEasyNotMovingLikeMedium) {
                return CheckResult.wrong("Looks like your Easy level AI doesn't make a random move!");
            }
            return CheckResult.correct();
        }


        @DynamicTest(order = 6)
        CheckResult checkEasyVsEasy() {

            TestedProgram program = new TestedProgram();
            program.start();

            String output = program.execute("start easy easy");

            List<Grid> gridList = Grid.allGridsFromOutput(output);

            Grid.checkGridSequence(gridList);

            return CheckResult.correct();
        }

        @DynamicTest(repeat = 10, order = 7)
        CheckResult checkMediumAi() {
            TestedProgram program = new TestedProgram();
            program.start();
            program.execute("start user medium");

            String output = program.execute("2 2");

            Grid gameGrid = Grid.fromOutput(output, 2);

            CellState[][] cellStates = gameGrid.getGrid();

            if (cellStates[0][0] == CellState.EMPTY && cellStates[2][2] == CellState.EMPTY) {
                output = program.execute("1 1");
                gameGrid = Grid.fromOutput(output, 2);
                if (gameGrid.getGrid()[2][2] == CellState.EMPTY) {
                    return CheckResult.wrong("Looks like your Medium level AI doesn't make a correct move!");
                }
            } else {
                output = program.execute("1 3");
                gameGrid = Grid.fromOutput(output, 2);
                if (gameGrid.getGrid()[2][0] == CellState.EMPTY) {
                    return CheckResult.wrong("Looks like your Medium level AI doesn't make a correct move!");
                }
            }
            program.stop();

            return CheckResult.correct();
        }

        @DynamicTest(order = 8, repeat = 5)
        CheckResult checkMediumVsMedium() {

            TestedProgram program = new TestedProgram();
            program.start();

            String output = program.execute("start medium medium");

            List<Grid> gridList = Grid.allGridsFromOutput(output);

            Grid.checkGridSequence(gridList);

            return CheckResult.correct();
        }

        boolean isMediumNotMovingLikeHard = false;

        @DynamicTest(repeat = 30, order = 9)
        CheckResult checkMediumNotMovingLikeHard() {

            if (isMediumNotMovingLikeHard) {
                return CheckResult.correct();
            }

            TestedProgram program = new TestedProgram();
            program.start();

            program.execute("start user medium");

            String output = program.execute("2 2");

            Grid userMoveGrid = Grid.fromOutput(output, 1);
            Grid mediumMoveGrid = Grid.fromOutput(output, 2);

            Position mediumMove = Grid.getMove(userMoveGrid, mediumMoveGrid);

            List<Position> minimaxCorrectPositions = Minimax.getAvailablePositions(userMoveGrid, CellState.O);

            if (!minimaxCorrectPositions.contains(mediumMove)) {
                isMediumNotMovingLikeHard = true;
            }

            return CheckResult.correct();
        }

        @DynamicTest(order = 10)
        CheckResult checkMediumNotMovingLikeHardAfter() {
            if (!isMediumNotMovingLikeHard) {
                return CheckResult.wrong("Looks like Medium level AI doesn't make a random move!");
            }
            return CheckResult.correct();
        }
    }
  learner_created: false
- name: src/tictactoe/domain/Board.java
  visible: true
  text: |
    package tictactoe.domain;

    import tictactoe.domain.constant.GameState;

    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;
    import java.util.stream.Collectors;

    public class Board {

        private final List<List<String>> state;

        public Board(String initialState) {
            this.state = initializeBoard(initialState);
        }

        public List<List<String>> getState() {
            return state;
        }

        public int getBoardSize() {
            return this.state.size();
        }

        public GameState isFinished() {
            if (existRow() != GameState.GAME_NOT_FINISHED) {
                return existRow();
            }

            if (existColumn() != GameState.GAME_NOT_FINISHED) {
                return existColumn();
            }

            if (existRightDiagonal() != GameState.GAME_NOT_FINISHED) {
                return existRightDiagonal();
            }

            if (existLeftDiagonal() != GameState.GAME_NOT_FINISHED) {
                return existLeftDiagonal();
            }

            if (isDraw()) {
                return GameState.DRAW;
            }

            return GameState.GAME_NOT_FINISHED;
        }

        public void showBoard() {
            System.out.println("---------");
            for (int i = 0; i < getBoardSize(); i++) {
                System.out.print("| ");
                for (int j = 0; j < getBoardSize(); j++) {
                    System.out.print(this.state.get(i).get(j) + " ");
                }
                System.out.println("|");
            }
            System.out.println("---------");
        }

        public void update(int rowIndex, int colIndex, String label) {
            state.get(rowIndex - 1)
                    .set(colIndex - 1, label);
        }

        private boolean isDraw() {
            return countSpaceString() == 0;
        }

        private GameState existLeftDiagonal() {
            List<String> strings = new ArrayList<>();
            for (int row = 0; row < state.size(); row++) {
                strings.add(state.get(row).get(row));
            }

            return whoWins(strings);
        }

        private GameState existRightDiagonal() {
            List<String> strings = new ArrayList<>();
            for (int row = 0; row < state.size(); row++) {
                strings.add(state.get(row).get(state.size() - 1 - row));
            }

            return whoWins(strings);
        }

        private GameState existColumn() {
            List<String> columnBoard = new ArrayList<>();
            for (int col = 0; col < state.size(); col++) {
                columnBoard = getColumn(col, state);
                if (isChecked(columnBoard)) {
                    break;
                }
            }

            return whoWins(columnBoard);
        }

        private GameState existRow() {
            List<String> rowBoard = new ArrayList<>();
            for (int row = 0; row < state.size(); row++) {
                rowBoard = getRow(row, state);
                if (isChecked(rowBoard)) {
                    break;
                }
            }

            return whoWins(rowBoard);
        }

        private List<String> getColumn(int columnIndex, List<List<String>> source) {
            List<String> column = new ArrayList<>();
            for (List<String> strings : source) {
                column.add(strings.get(columnIndex));
            }
            return column;
        }

        private List<String> getRow(int rowIndex, List<List<String>> source) {
            return new ArrayList<>(source.get(rowIndex));
        }

        private boolean isChecked(List<String> data) {
            return data.stream().distinct().count() == 1;
        }

        private GameState whoWins(List<String> strings) {
            if (String.join("", strings).equals("XXX")) {
                return GameState.X_WINS;
            }

            if (String.join("", strings).equals("OOO")) {
                return GameState.O_WINS;
            }

            return GameState.GAME_NOT_FINISHED;
        }

        private List<List<String>> initializeBoard(String initialState) {
            List<List<String>> result = Arrays.asList(
                    Arrays.asList("", "", ""),
                    Arrays.asList("", "", ""),
                    Arrays.asList("", "", "")
            );

            byte[] bytes = initialState.getBytes();
            int row = 0;
            int col = 0;

            for (int i = 0; i < bytes.length; i++) {
                result.get(row).set(col, String.valueOf((char) bytes[i]));
                col++;
                if (isNextRow(i)) {
                    result.set(row, result.get(row).stream()
                            .map(v -> {
                                if (v.equals("_")) {
                                    return " ";
                                }
                                return v;
                            }).collect(Collectors.toList()));
                    row++;
                    col = 0;

                }
            }

            return result;
        }

        private boolean isNextRow(int i) {
            return (i + 1) % 3 == 0;
        }

        private int countSpaceString() {
            int count = 0;
            for (int row = 0; row < state.size(); row++) {
                for (int col = 0; col < state.size(); col++) {
                    if (state.get(row).get(col).equals(" ")) {
                        count++;
                    }
                }
            }
            return count;
        }
    }
  learner_created: true
- name: src/tictactoe/domain/BoardProcessor.java
  visible: true
  text: "package tictactoe.domain;\n\nimport tictactoe.domain.constant.ApplicationState;\n\
    import tictactoe.domain.constant.GameState;\nimport tictactoe.domain.constant.InputState;\n\
    import tictactoe.domain.player.Player;\nimport tictactoe.domain.player.PlayerMachine;\n\
    import tictactoe.domain.utils.UserInputHandler;\n\nimport java.util.List;\n\n\
    import static tictactoe.domain.utils.UserInputHandler.scanner;\n\npublic class\
    \ BoardProcessor {\n\n    private static final String SPACE_STRING = \" \";\n\
    \    private final Board board;\n\n    public BoardProcessor(String initialState)\
    \ {\n        this.board = new Board(initialState);\n    }\n\n    public Board\
    \ getBoard() {\n        return board;\n    }\n\n    public void showBoard() {\n\
    \        this.board.showBoard();\n    }\n\n    public ApplicationState updateBoard(Player\
    \ playerA, Player playerB) {\n        if (isPlayerAbleToMakeANewMove()) {\n  \
    \          makeAMove(playerA);\n        }\n        \n        if (isPlayerAbleToMakeANewMove())\
    \ {\n            makeAMove(playerB);\n        }\n\n        return checkState();\n\
    \    }\n\n    private boolean isPlayerAbleToMakeANewMove() {\n        return board.isFinished()\
    \ == GameState.GAME_NOT_FINISHED;\n    }\n\n    private void makeAMove(Player\
    \ player) {\n        List<Integer> coordinates;\n        InputState inputState;\n\
    \n        if (player instanceof PlayerMachine) {\n            System.out.println(\"\
    Making move level \\\"easy\\\"\");\n            do {\n                coordinates\
    \ = player.getCoordinates();\n            } while (!isValidRandomCoordinates(coordinates));\n\
    \        } else {\n            String inputCoordinates;\n            do {\n  \
    \              System.out.print(\"Enter the coordinates: \");\n              \
    \  inputCoordinates = scanner.nextLine();\n                inputState = UserInputHandler.checkInPutCoordinates(this,\
    \ inputCoordinates);\n            } while (inputState != InputState.VALID);\n\
    \            player.setCoordinates(inputCoordinates);\n            coordinates\
    \ = player.getCoordinates();\n        }\n\n        board.update(coordinates.get(0),\
    \ coordinates.get(1), player.getLabel());\n        showBoard();\n    }\n\n   \
    \ private ApplicationState checkState() {\n        if (this.board.isFinished()\
    \ == GameState.X_WINS) {\n            System.out.println(\"X wins\");\n      \
    \      return ApplicationState.EXIT;\n        }\n        if (this.board.isFinished()\
    \ == GameState.O_WINS) {\n            System.out.println(\"O wins\");\n      \
    \      return ApplicationState.EXIT;\n        }\n        if (this.board.isFinished()\
    \ == GameState.DRAW) {\n            System.out.println(\"Draw\");\n          \
    \  return ApplicationState.EXIT;\n        }\n        return ApplicationState.CONTINUE;\n\
    \    }\n\n    private boolean isValidRandomCoordinates(List<Integer> coordinates)\
    \ {\n        // A valid random coordinates is a place where not occupied\n   \
    \     return board.getState().get(coordinates.get(0) - 1).get(coordinates.get(1)\
    \ - 1).equals(SPACE_STRING);\n    }\n}\n"
  learner_created: true
- name: src/tictactoe/domain/player/PlayerMachine.java
  visible: true
  text: |
    package tictactoe.domain.player;

    import java.util.ArrayList;
    import java.util.List;
    import java.util.Random;

    public class PlayerMachine implements Player{
        private final String label;
        private long seed;

        public PlayerMachine(String label) {
            this.label = label;
            seed = System.nanoTime();
        }

        @Override
        public void setCoordinates(String coordinates) {

        }

        @Override
        public List<Integer> getCoordinates() {
            return randomizeCoordinates();
        }

        @Override
        public String getLabel() {
            return label;
        }

        private List<Integer> randomizeCoordinates() {
            List<Integer> coordinates = new ArrayList<>();
            seed = System.nanoTime();
            Random random = new Random(seed);
            int x = random.nextInt(3) + 1;
            int y = random.nextInt(3) + 1;
            coordinates.add(x);
            coordinates.add(y);
            return coordinates;
        }
    }
  learner_created: true
- name: src/tictactoe/domain/utils/UserInputHandler.java
  visible: true
  text: |
    package tictactoe.domain.utils;

    import tictactoe.domain.BoardProcessor;
    import tictactoe.domain.constant.ApplicationState;
    import tictactoe.domain.constant.InputState;

    import java.util.Optional;
    import java.util.Scanner;

    public class UserInputHandler {
        static final String SPACE_STRING = " ";
        static final String EMPTY_STRING = "";
        static final String BAD_PARAMETERS_MSG = "Bad parameters!";

        public static final Scanner scanner = new Scanner(System.in);

        public static InputState checkInPutCoordinates(BoardProcessor boardProcessor, String inputCoordinates) {
            String[] inputs = inputCoordinates.split(SPACE_STRING);
            InputState inputState = InputState.VALID;

            Optional<InputState> invalidInput = isInvalidInput(inputs);
            if (invalidInput.isPresent()) return invalidInput.get();

            Optional<InputState> outOfRangeInput = isOutOfRangeInput(inputs);
            if (outOfRangeInput.isPresent()) return outOfRangeInput.get();

            Optional<InputState> occupiedInput = isOccupiedInput(inputs, boardProcessor);
            if (occupiedInput.isPresent()) return occupiedInput.get();

            return inputState;
        }

        public static ApplicationState handleArguments() {
            System.out.print("Input command: ");
            String[] arguments = scanner.nextLine().split(" ");
            if (arguments[0].equals("exit")) {
                return ApplicationState.EXIT;
            }

            if (
                    arguments.length != 3 ||
                            !arguments[0].equals("start") ||
                            (!arguments[1].equals("user") && !arguments[1].equals("easy")) ||
                            (!arguments[2].equals("user") && !arguments[2].equals("easy"))
            ) {
                System.out.println(BAD_PARAMETERS_MSG);
                return ApplicationState.BAD_PARAMETER;
            }

            if (arguments[1].equals("user") && arguments[2].equals("easy")) {
                return ApplicationState.USER_EASY;
            }

            if (arguments[1].equals("easy") && arguments[2].equals("user")) {
                return ApplicationState.EASY_USER;
            }

            if (arguments[1].equals("easy") && arguments[2].equals("easy")) {
                return ApplicationState.EASY_EASY;
            }

            if (arguments[1].equals("user") && arguments[2].equals("user")) {
                return ApplicationState.USER_USER;
            }

            return ApplicationState.START;
        }

        private static Optional<InputState> isOccupiedInput(String[] inputs, BoardProcessor boardProcessor) {
            if (!isAvailable(inputs, boardProcessor)) {
                System.out.println("This cell is occupied! Choose another one!");
                return Optional.of(InputState.OCCUPIED);
            }
            return Optional.empty();
        }

        private static Optional<InputState> isOutOfRangeInput(String[] inputs) {
            for (String input : inputs) {
                if (!isInRange(input)) {
                    System.out.println("Coordinates should be from 1 to 3!");
                    return Optional.of(InputState.OUT_OF_RANGE);
                }
            }
            return Optional.empty();
        }

        private static Optional<InputState> isInvalidInput(String[] inputs) {
            for (String input : inputs) {
                if (!isNumeric(input)) {
                    System.out.println("You should enter numbers!");
                    return Optional.of(InputState.INVALID_INPUT);
                }
            }
            return Optional.empty();
        }

        private static boolean isAvailable(String[] inputs, BoardProcessor boardProcessor) {
            int row = Integer.parseInt(inputs[0]);
            int col = Integer.parseInt(inputs[1]);

            String queryingValue;
            try {
                queryingValue = boardProcessor.getBoard().getState().get(row - 1).get(col - 1);
            } catch (IndexOutOfBoundsException e) {
                return false;
            }
            return isSpaceString(queryingValue);
        }

        private static boolean isInRange(String input) {
            int index = Integer.parseInt(input) - 1;
            return index < 3 && index >= 0;
        }

        private static boolean isNumeric(String input) {
            if (input == null || isEmptyString(input)) {
                return false;
            }
            try {
                Double.parseDouble(input);
            } catch (NumberFormatException e) {
                return false;
            }
            return true;
        }

        private static boolean isSpaceString(String value) {
            return value.equals(SPACE_STRING);
        }

        private static boolean isEmptyString(String value) {
            return value.equals(EMPTY_STRING);
        }
    }
  learner_created: true
- name: src/tictactoe/domain/constant/GameState.java
  visible: true
  text: |
    package tictactoe.domain.constant;

    public enum GameState {
        GAME_NOT_FINISHED,
        DRAW,
        X_WINS,
        O_WINS;
    }
  learner_created: true
- name: src/tictactoe/domain/constant/InputState.java
  visible: true
  text: |
    package tictactoe.domain.constant;

    public enum InputState {
        INVALID_INPUT,
        OUT_OF_RANGE,
        OCCUPIED,
        VALID;
    }
  learner_created: true
- name: src/tictactoe/domain/constant/ApplicationState.java
  visible: true
  text: |
    package tictactoe.domain.constant;

    public enum ApplicationState {
        EXIT,
        BAD_PARAMETER,
        START,
        USER_USER,
        USER_EASY,
        EASY_USER,
        EASY_EASY,
        CONTINUE;
    }
  learner_created: true
- name: src/tictactoe/domain/player/Player.java
  visible: true
  text: |
    package tictactoe.domain.player;


    import java.util.List;

    public interface Player {
        void setCoordinates(String coordinates);
        List<Integer> getCoordinates();
        String getLabel();
    }
  learner_created: true
- name: src/tictactoe/domain/player/PlayerUser.java
  visible: true
  text: |
    package tictactoe.domain.player;

    import java.util.ArrayList;
    import java.util.List;

    public class PlayerUser implements Player {
        private final String label;
        private String coordinates;

        public PlayerUser(String coordinates, String label) {
            this.label = label;
            this.coordinates = coordinates;
        }

        @Override
        public void setCoordinates(String coordinates) {
            this.coordinates = coordinates;
        }

        @Override
        public List<Integer> getCoordinates() {
            String[] inputs = coordinates.split(" ");
            return new ArrayList<>(
                    List.of(Integer.parseInt(inputs[0]), Integer.parseInt(inputs[1]))
            );
        }

        @Override
        public String getLabel() {
            return label;
        }

    }
  learner_created: true
- name: src/tictactoe/domain/constant/GameLevel.java
  visible: true
  learner_created: true
- name: src/tictactoe/domain/utils/TwoDimListHandler.java
  visible: true
  learner_created: true
feedback_link: https://hyperskill.org/learn/step/7437#comment
status: Solved
feedback:
  message: Congratulations!
  time: Sat, 25 Dec 2021 15:56:33 UTC
record: 4
