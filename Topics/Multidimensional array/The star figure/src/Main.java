import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        List<List<String>> matrix = constructMatrix(size);
        printResult(matrix);
    }

    private static void printResult(List<List<String>> result) {
        for (int row = 0; row < result.size(); row++) {
            for (int col = 0; col < result.size(); col++) {
                System.out.print(result.get(row).get(col) + " ");
            }
            System.out.println();
        }
    }

    private static List<List<String>> constructMatrix(int size) {
        int mark = size / 2;
        List<List<String>> initializeMatrix = initializeMatrix(size);
        List<List<String>> filledDiagonalMatrix = fillDiagonal(initializeMatrix);
        List<List<String>> lists = fillVerticalHorizontal(filledDiagonalMatrix, mark);
        return lists;
    }

    private static List<List<String>> fillVerticalHorizontal(List<List<String>> matrix, int mark) {
        List<List<String>> result = new ArrayList<>(matrix);
        int size = matrix.size();

        for (int index = 0; index < size; index++) {
            result.get(index).set(mark, "*");
            result.get(mark).set(index, "*");
        }
        return result;
    }

    private static List<List<String>> fillDiagonal(List<List<String>> matrix) {
        List<List<String>> result = new ArrayList<>(matrix);
        int size = matrix.size();

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if (row == col || row + col == size - 1) {
                    result.get(row).set(col, "*");
                }
            }
        }
        return result;
    }

    private static List<List<String>> initializeMatrix(int size) {
        List<List<String>> ret = new ArrayList<>();
        for (int row = 0; row < size; row++) {
            ret.add(new ArrayList<>(Collections.nCopies(size, ".")));
        }
        return ret;
    }
}
