import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        String[] numbers = scanner.nextLine().split(" ");
        int move = scanner.nextInt();

        move = calculateCircularIndex(numbers.length, move);

        String[] result = rotateRight(numbers, move);
        for (String r : result) {
            System.out.print(r + " ");
        }
    }

    private static int calculateCircularIndex(int circularSize, int move) {
        return move % circularSize;
    }

    private static String[] rotateRight(String[] numbers, int move) {
        int length = numbers.length;
        int offset = length - move;

        String[] temp1 = Arrays.copyOfRange(numbers, 0, offset);
        String[] temp2 = Arrays.copyOfRange(numbers, offset, length);

        return Stream.concat(Arrays.stream(temp2), Arrays.stream(temp1)).toArray(String[]::new);
    }
}
