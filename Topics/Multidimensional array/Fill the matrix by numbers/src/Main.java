import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        List<List<Integer>> result = constructMatrix(size);

        printResult(result);
    }

    private static void printResult(List<List<Integer>> result) {
        for (int row = 0; row < result.size(); row++) {
            for (int col = 0; col < result.size(); col++) {
                System.out.print(result.get(row).get(col) + " ");
            }
            System.out.println();
        }
    }

    private static List<List<Integer>> constructMatrix(int matrixSize) {
        List<List<Integer>> ret = new ArrayList<>();

        for (int i = 0; i < matrixSize; i++) {
            List<Integer> rightList = constructRightList(matrixSize, i);
            List<Integer> leftList = constructLeftList(i);
            ret.add(
                    Stream.concat(
                                    leftList.stream(),
                                    rightList.stream()
                            )
                            .collect(Collectors.toList()));

        }
        return ret;
    }

    private static List<Integer> constructLeftList(int i) {
        List<Integer> leftList = IntStream.range(1, i + 1).boxed().collect(Collectors.toList());
        Collections.reverse(leftList);
        return leftList;
    }

    private static List<Integer> constructRightList(int matrixSize, int i) {
        return IntStream.range(0, matrixSize - i).boxed().collect(Collectors.toList());
    }
}
