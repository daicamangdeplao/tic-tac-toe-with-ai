import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ArrayOperations {
//    public static void main(String[] args) {
//        int[][] twoDimArray = {
//                {0, 0, 9, 9},
//                {1, 2, 3, 4},
//                {5, 6, 7, 8}
//        };
//        reverseElements(twoDimArray);
//    }

    public static void reverseElements(int[][] twoDimArray) {
        // write your code here
        List<List<Integer>> twoDimList = convertToList(twoDimArray);
        List<List<Integer>> reversedTwoDimList = proceed(twoDimList);
//        twoDimArray = convertToArray(reversedTwoDimList);
        for (int i = 0; i < reversedTwoDimList.size(); i++) {
            for (int j = 0; j < reversedTwoDimList.get(0).size(); j++) {
                twoDimArray[i][j] = reversedTwoDimList.get(i).get(j);
            }
        }
        System.out.println();

    }

    private static int[][] convertToArray(List<List<Integer>> reversedTwoDimList) {
        int[][] result = new int[reversedTwoDimList.size()][reversedTwoDimList.get(0).size()];
        for (int i = 0; i < reversedTwoDimList.size(); i++) {
            for (int j = 0; j < reversedTwoDimList.get(0).size(); j++) {
                result[i][j] = reversedTwoDimList.get(i).get(j);
            }
        }
        return result;
    }

    private static List<List<Integer>> proceed(List<List<Integer>> twoDimList) {
        for (List<Integer> l :twoDimList) {
            Collections.reverse(l);
        }
        return twoDimList;
    }

    private static List<List<Integer>> convertToList(int[][] twoDimArray) {
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < twoDimArray.length; i++) {
            ArrayList<Integer> collect = Arrays.stream(twoDimArray[i]).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
            result.add(collect);
        }
        return result;
    }
}
