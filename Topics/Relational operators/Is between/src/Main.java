import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here
        int number = scanner.nextInt();
        int lower = scanner.nextInt();
        int upper = scanner.nextInt();

        if (upper < lower) {
            int temp = upper;
            upper = lower;
            lower = temp;
        }

        if (belongToTheRange(number, lower, upper)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }

    private static boolean belongToTheRange(int numberInclusive, int lower, int upper) {
        return numberInclusive <= upper && numberInclusive >= lower;
    }
}
