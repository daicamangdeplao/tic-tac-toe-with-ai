import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here
        final String input = scanner.next();
        int sum = 0;

        for (int i = 0; i < input.length(); i++) {
            sum = sum + Integer.parseInt(String.valueOf(input.charAt(i)));
        }

        System.out.println(sum);

    }
}
