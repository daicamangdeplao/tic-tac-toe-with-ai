import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // write your code here
        long K = scanner.nextLong();
        long N = scanner.nextLong();
        double M = scanner.nextDouble();

        boolean solved = false;
        while (true) {
            Random random = new Random(K);
            for (int i = 0; i < N; i++) {
                double randomGaussianNumber = random.nextGaussian();
                if (randomGaussianNumber > M) {
                    K++;
                    break;
                }
                // TODO is loop over?
                if (i == N - 1) {
                    solved = true;
                }
            }
            if (solved) break;
        }

        System.out.println(K);
    }
}
