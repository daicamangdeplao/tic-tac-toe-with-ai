package tictactoe.domain;

import tictactoe.domain.constant.GameState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Board {

    private final List<List<String>> state;

    public Board(String initialState) {
        this.state = initializeBoard(initialState);
    }

    public List<List<String>> getState() {
        return state;
    }

    public int getBoardSize() {
        return this.state.size();
    }

    public GameState isFinished() {
        if (existRow() != GameState.GAME_NOT_FINISHED) {
            return existRow();
        }

        if (existColumn() != GameState.GAME_NOT_FINISHED) {
            return existColumn();
        }

        if (existRightDiagonal() != GameState.GAME_NOT_FINISHED) {
            return existRightDiagonal();
        }

        if (existLeftDiagonal() != GameState.GAME_NOT_FINISHED) {
            return existLeftDiagonal();
        }

        if (isDraw()) {
            return GameState.DRAW;
        }

        return GameState.GAME_NOT_FINISHED;
    }

    public void showBoard() {
        System.out.println("---------");
        for (int i = 0; i < getBoardSize(); i++) {
            System.out.print("| ");
            for (int j = 0; j < getBoardSize(); j++) {
                System.out.print(this.state.get(i).get(j) + " ");
            }
            System.out.println("|");
        }
        System.out.println("---------");
    }

    public void update(int rowIndex, int colIndex, String label) {
        state.get(rowIndex)
                .set(colIndex, label);
    }

    private boolean isDraw() {
        return countSpaceString() == 0;
    }

    private GameState existLeftDiagonal() {
        List<String> strings = new ArrayList<>();
        for (int row = 0; row < state.size(); row++) {
            strings.add(state.get(row).get(row));
        }

        return whoWins(strings);
    }

    private GameState existRightDiagonal() {
        List<String> strings = new ArrayList<>();
        for (int row = 0; row < state.size(); row++) {
            strings.add(state.get(row).get(state.size() - 1 - row));
        }

        return whoWins(strings);
    }

    private GameState existColumn() {
        List<String> columnBoard = new ArrayList<>();
        for (int col = 0; col < state.size(); col++) {
            columnBoard = getColumn(col, state);
            if (isChecked(columnBoard)) {
                break;
            }
        }

        return whoWins(columnBoard);
    }

    private GameState existRow() {
        List<String> rowBoard = new ArrayList<>();
        for (int row = 0; row < state.size(); row++) {
            rowBoard = getRow(row, state);
            if (isChecked(rowBoard)) {
                break;
            }
        }

        return whoWins(rowBoard);
    }

    private List<String> getColumn(int columnIndex, List<List<String>> source) {
        List<String> column = new ArrayList<>();
        for (List<String> strings : source) {
            column.add(strings.get(columnIndex));
        }
        return column;
    }

    private List<String> getRow(int rowIndex, List<List<String>> source) {
        return new ArrayList<>(source.get(rowIndex));
    }

    private boolean isChecked(List<String> data) {
        return data.stream().distinct().count() == 1;
    }

    private GameState whoWins(List<String> strings) {
        if (String.join("", strings).equals("XXX")) {
            return GameState.X_WINS;
        }

        if (String.join("", strings).equals("OOO")) {
            return GameState.O_WINS;
        }

        return GameState.GAME_NOT_FINISHED;
    }

    private List<List<String>> initializeBoard(String initialState) {
        List<List<String>> result = Arrays.asList(
                Arrays.asList("", "", ""),
                Arrays.asList("", "", ""),
                Arrays.asList("", "", "")
        );

        byte[] bytes = initialState.getBytes();
        int row = 0;
        int col = 0;

        for (int i = 0; i < bytes.length; i++) {
            result.get(row).set(col, String.valueOf((char) bytes[i]));
            col++;
            if (isNextRow(i)) {
                result.set(row, result.get(row).stream()
                        .map(v -> {
                            if (v.equals("_")) {
                                return " ";
                            }
                            return v;
                        }).collect(Collectors.toList()));
                row++;
                col = 0;

            }
        }

        return result;
    }

    private boolean isNextRow(int i) {
        return (i + 1) % 3 == 0;
    }

    private int countSpaceString() {
        int count = 0;
        for (int row = 0; row < state.size(); row++) {
            for (int col = 0; col < state.size(); col++) {
                if (state.get(row).get(col).equals(" ")) {
                    count++;
                }
            }
        }
        return count;
    }
}
